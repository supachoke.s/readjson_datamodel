import 'dart:convert';

// To parse this JSON data, do
//
//     final productDataModel = productDataModelFromJson(jsonString);

List<ProductDataModel> productDataModelFromJson(String str) =>
    List<ProductDataModel>.from(
        json.decode(str).map((x) => ProductDataModel.fromJson(x)));

String productDataModelToJson(List<ProductDataModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ProductDataModel {
  ProductDataModel({
    required this.id,
    required this.name,
    required this.category,
    required this.imageUrl,
    required this.oldPrice,
    required this.price,
  });

  int? id;
  final String name;
  final String category;
  final String imageUrl;
  final String oldPrice;
  final String price;

  factory ProductDataModel.fromJson(Map<String, dynamic> json) =>
      ProductDataModel(
        id: json["id"],
        name: json["name"],
        category: json["category"],
        imageUrl: json["imageUrl"],
        oldPrice: json["oldPrice"],
        price: json["price"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "category": category,
        "imageUrl": imageUrl,
        "oldPrice": oldPrice,
        "price": price,
      };
}
